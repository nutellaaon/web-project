<?php
include('config/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
  </head>
<body>
      <div class="content">
        <nav class="navbar" role="navigation">
            <h2 class="mt-0">Product List</h2>
            <ul class="nav navbar-nav navbar-right align-bottom">
              <li>
                  <button type="button" class="btn btn-outline-dark">ADD</button>
                  <button type="button" class="btn btn-outline-dark ">MASS DELETE</button>
              </li>
            </ul>
        </nav>
        <hr>
        <div class="row">
          <div class="col-3">
            <div class="card">
              <div class="card-body">
                <input class=".delete-checkbox" type="checkbox" value="" id="flexCheckDefault">
                <p class="text-center">Some quick exa card's content.</p>
                <p class="text-center">Some quick exa card's content.</p>
                <p class="text-center">Some quick exa card's content.</p>
                <p class="text-center">Some quick exa card's content.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer id="footer">
        <hr>
        <p class="text-center">Scandiweb Test assignment</p>
      </footer>
</body>
</html>