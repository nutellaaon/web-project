<?php
include('config/config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  </head>
<body>
  <div class="content">
      <nav class="navbar" role="navigation">
          <h2 class="mt-0">Product List</h2>
          <ul class="nav navbar-nav navbar-right align-bottom">
            <li>
                <button type="button" class="btn btn-outline-dark">Save</button>
                <button type="button" class="btn btn-outline-dark ">Cancel</button>
            </li>
          </ul>
      </nav>
      <hr>
      <form id="#product_form">
        <div class="form-group row">
          <label class="col-md-1 col-form-label">SKU</label>
          <div class="col-md-3">
            <input type="text" class="form-control" id="#sku" placeholder=#sku>
          </div>
        </div>
        <div class="form-group row">
            <label class="col-md-1 col-form-label">Name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="#name" placeholder=#name>
            </div>
        </div>
          <div class="form-group row">
            <label class="col-md-1 col-form-label">Price ($)</label>
            <div class="col-md-3">
              <input type="number" class="form-control" id="#price" placeholder=#price>
            </div>
          </div>
        
        
        <div class="form-group row" id="type">
            <label class="col-md-2 col-form-label">Type switcher</label>
            <div class="col-md-2">
                <select class="form-control" id="productType">
                    <option selected hidden>Type switcher</option>
                    <option value="DVD">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                  </select>
            </div>
        </div>
        
        <div class="container-fluid" id="showDVD">
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Size (MB)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" id="#size" placeholder=#size>
                </div>
            </div>
            <p class="">*Please provide DVD Size in Decimal*</p>
        </div>
        <div class="container-fluid" id="showFurniture">
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Height (CM)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" id="#height" placeholder=#height>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Width (CM)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" id="#width" placeholder=#width>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Length (CM)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" id="#length" placeholder=#length>
                </div>
            </div>
            <p class="">*Please provide dimension in HxWxL format*</p>
        </div>
        <div class="container-fluid" id="showBook">
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Weight (KG)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" id="#weight" placeholder=#weight>
                </div>
            </div>
            <p class="">*Please provide Book Weight in KG*</p>
        </div>      
      </form>
    </div>
      <footer id="footer">
        <hr>
        <p class="text-center">Scandiweb Test assignment</p>
      </footer>
      <script>
        $(document).ready(function(){
          $("#productType").on('change', function(){
            var value = $(this).val(); 
              $("div.container-fluid").hide();
              $("#show"+value).show();
          });
        });
      </script>
</body>
</html>